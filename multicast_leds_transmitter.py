import time
import board
import neopixel
import random
import math
import socket
from random import randrange

# Multicast code adapted from https://huichen-cs.github.io/course/CISC7334X/20FA/lecture/pymcast/

# On a Raspberry pi, use this instead, not all pins are supported
pixel_pin = board.D18

# The number of NeoPixels
num_pixels   = 100
num_pixels_x = 10
num_pixels_y = 10

# The order of the pixel colors - RGB or GRB. Some NeoPixels have red and green reversed!
# For RGBW NeoPixels, simply change the ORDER to RGBW or GRBW.
ORDER = neopixel.GRB

pixels = neopixel.NeoPixel(
    #pixel_pin, num_pixels, brightness=0.2, auto_write=True, pixel_order=ORDER
    pixel_pin, num_pixels, brightness=0.1, auto_write=False, pixel_order=ORDER
)

def reset_pixels():
    pixels.fill((0,0,0))
    pixels.show()

def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        r = g = b = 0
    elif pos < 85:
        r = int(pos * 3)
        g = int(255 - pos * 3)
        b = 0
    elif pos < 170:
        pos -= 85
        r = int(255 - pos * 3)
        g = 0
        b = int(pos * 3)
    else:
        pos -= 170
        r = 0
        g = int(pos * 3)
        b = int(255 - pos * 3)
    return (r, g, b) if ORDER in (neopixel.RGB, neopixel.GRB) else (r, g, b, 0)

def mc_send(hostip, mcgrpip, mcport, msgbuf):
    sender = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM, \
            proto=socket.IPPROTO_UDP, fileno=None)
    mcgrp = (mcgrpip, mcport)
    sender.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 1)
    sender.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_IF, \
             socket.inet_aton(hostip))

    sender.sendto(msgbuf, mcgrp)
    sender.close()

# Sets all pixels to the same colour (red to start, then randomizes)
def mc_solid():
    color=(255,0,0)
    while 1:
        bcolor=color[0].to_bytes(1,'big')+color[1].to_bytes(1,'big')+color[2].to_bytes(1,'big')
        mc_send("0.0.0.0","224.0.0.15",50019,bcolor)
        for i in range(num_pixels):
           pixels[i]=color
        pixels.show()
        time.sleep(5)
        color=wheel(randrange(255))

while True:
    mc_solid()
