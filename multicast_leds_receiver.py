import time
import board
import neopixel
from random import randrange
import sys
import socket
import struct

# On a Raspberry pi, use this instead, not all pins are supported
pixel_pin = board.D18

# The number of NeoPixels
num_pixels = 300

# The order of the pixel colors - RGB or GRB. Some NeoPixels have red and green reversed!
# For RGBW NeoPixels, simply change the ORDER to RGBW or GRBW.
ORDER = neopixel.RGB

pixels = neopixel.NeoPixel(
    pixel_pin, num_pixels, brightness=0.2, auto_write=False, pixel_order=ORDER
)

def mc_solid():
    while 1:
        msg = mc_recv('0.0.0.0', '224.0.0.15', 50019)
        color1=int.from_bytes(msg[0:1],'big')
        color2=int.from_bytes(msg[1:2],'big')
        color3=int.from_bytes(msg[2:3],'big')
        color=(color1,color2,color3)
#        print(color)
        for i in range(num_pixels):
           pixels[i]=color
        pixels.show()

def mc_recv(fromnicip, mcgrpip, mcport):
    bufsize = 1024

    receiver = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM, \
            proto=socket.IPPROTO_UDP, fileno=None)

    bindaddr = (mcgrpip, mcport)
    receiver.bind(bindaddr)

    if fromnicip == '0.0.0.0':
        mreq = struct.pack("=4sl", socket.inet_aton(mcgrpip), socket.INADDR_ANY)
    else:
        mreq = struct.pack("=4s4s", \
            socket.inet_aton(mcgrpip), socket.inet_aton(fromnicip))
    receiver.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

    buf, senderaddr = receiver.recvfrom(1024)
    msg = buf

    receiver.close()

    return msg

while True:
    pixels.fill((0,0,0))
    pixels.show()
    mc_solid()
